app.factory('MasterLayout_Services', function ($http) {

    return {
        get_list: function (url) {
            return $http.get(url).then(function (data) {
                return data.data;
            });
        },
        post_list: function (url, obj) {
            return $http.post(url, obj).then(function (data) {
                return data;
            });
        },
        notification: function (title, message) {
            if (Notification.permission !== "granted") {
                Notification.requestPermission();
            } else {
                console.log(Notification);
                var notification = new Notification(title, {
                    body: message,
                    icon: "json-formatter-master/screenshot.png",                    
                    // image : "json-formatter-master/screenshot.png",                    
                });
                // notification.onclick = function () {
                //     window.open("http://google.com");
                // };
            }
            return notification;
        }
    };
});
