var app = angular.module('myApp', ['jsonFormatter']);
app.config(function (JSONFormatterConfigProvider) { // Enable the hover preview feature
    JSONFormatterConfigProvider.hoverPreviewEnabled = true;
    JSONFormatterConfigProvider.hoverPreviewArrayCount = false;
    JSONFormatterConfigProvider.hoverPreviewFieldCount = false;
});
app.controller('myCtrl', function ($scope, MasterLayout_Services, $http, $window) {
    $scope.get_initial_api_load = function () {
        MasterLayout_Services.get_list("model/get_data_api.php?show=get_project_folder_list").then(function (response) {
            $scope.folder = response.data;
        });
        MasterLayout_Services.get_list("model/get_data_api.php?show=get_command_list").then(function (response) {
            $scope.api_command = response.data;
        });
        // MasterLayout_Services.get_list("model/command.php?post=get_command_file_write").then(function (response) {
        //     // $scope.read_file = response;
        // });
    };
    $scope.get_initial_api_load();
    $scope.data = {
        'command_prompt': "stop"
    };
    $scope.select_folder = function (data) {
        console.log(data);
        MasterLayout_Services.post_list("model/command.php?post=run_command", data).then(function (response) {
            console.log(response.message, response.data.title);
            if (response.data.status == 'Success') {
                MasterLayout_Services.notification(response.data.title, response.data.message);
            }
        });
    };
    $scope.select_reset = function () {
        $scope.data = {};
        MasterLayout_Services.notification('Hello', 'Notification will successfully');
    }
    $scope.myFunction = function () {
        document.getElementById("demo").contentEditable = true;
    }
}).controller('get_header_request', function ($scope, MasterLayout_Services, $http, $window) {
    $scope.method_list = [
        {
            'method': 'Get',
            'value': 'GET'
        }, {
            'method': 'Post',
            'value': 'POST'
        }, {
            'method': 'Put',
            'value': 'PUT'
        }, {
            'method': 'Patch',
            'value': 'PATCH'
        }, {
            'method': 'Delete',
            'value': 'DELETE'
        },
    ]
    $scope.inital = function () {
        $scope.data = {
            'method': 'GET',
            'url': 'http://short_eats_club_php.local/api/'
        }
    }
    $scope.inital();
    $scope.personalDetails = [{
            'param': '',
            'value': ''
        }];
    $scope.canceled = false;
    $scope.addNew = function (personalDetail) {
        $scope.canceled = true;
        $scope.personalDetails.push({'param': '', 'value': ''});
    };
    $scope.reMove = function (index) {
        $scope.personalDetails.splice(index, 1);
        $scope.post_content($scope.personalDetails);
    };
    $scope.data.passing_url_data = '';
    $scope.post_api_change = function (variable) {
        $scope.data.passing_url_data = variable.url;
        $scope.post_content($scope.personalDetails);
    }
    $scope.post_content = function (array_value) {
        angular.forEach(array_value, function (obj, key) {
            if (key == 0) {
                if (obj.param != "") {
                    $scope.data.passing_url_data = $scope.data.url + '?' + obj.param;
                } else {
                    $scope.data.passing_url_data = $scope.data.url;
                }
                if (obj.value != "") {
                    $scope.data.passing_url_data = $scope.data.url + '?' + obj.param + '=' + obj.value;
                }
            } else {
                if (obj.param != "") {
                    $scope.data.passing_url_data = $scope.data.passing_url_data + '&' + obj.param;
                }
                if (obj.value != "") {
                    $scope.data.passing_url_data = $scope.data.passing_url_data + '=' + obj.value;
                }
            }
        });
    }
    $scope.select_method = function (data) {
        var config = {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + data.token,
                "accept": "application/json; charset=utf-8"
            }
        };
        $http({
            method: data.method,
            url: data.passing_url_data,
            headers: {
                'Authorization': 'Bearer ' + data.token,
                'Accept': 'application/json; charset=utf-8'
            }
        }).then(function (response) {
            $scope.json_variable = response.data;
            console.log(response);
        });
    }
    $scope.notify = function () {
        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        } else {
            console.log(Notification);
            var notification = new Notification('hello', {body: "Hey there!"});
            notification.onclick = function () {
                window.open("http://google.com");
            };
        }
    }
}).controller('get_component', function ($scope, MasterLayout_Services, $http, $window) { 
    $scope.datatypes = [
        {
            name: "integer",
            value: "integer"
        }, {
            name: "bigInteger",
            value: "bigInteger"
        }, {
            name: "string",
            value: "string"
        }, {
            name: "text",
            value: "text"
        }, {
            name: "date",
            value: "date"
        }, {
            name: "time",
            value: "time"
        }, {
            name: "decimal",
            value: "decimal"
        }, {
            name: "dateTime",
            value: "dateTime"
        }, {
            name: "boolean",
            value: "boolean"
        }, {
            name: "tinyInteger",
            value: "tinyInteger"
        }, {
            name: "time",
            value: "time"
        }, {
            name: "year",
            value: "year"
        }, {
            name: "string(255)",
            value: "full_length_string"
        }, {
            name: "Json value",
            value: "text_json"
        }
    ];
    $scope.inital = function () {
        $scope.table_columns = [{
                'column_name': '',
                'data_type': null,
                'comment': ''
            }];
    }
    $scope.inital();
    $scope.addNew = function (index) {
        $scope.table_columns.splice(index + 1, 0, {
            'column_name': '',
            'data_type': null,
            'comment': ''
        });
    };
    $scope.reMove = function (index) {
        $scope.table_columns.splice(index, 1);
    };
});
