<?php

switch ($_GET['show']) {
    case 'get_command_list':
        return get_command_list();
        break;
    case 'get_project_folder_list':
        return get_project_folder_list();
        break;
    case 'index':
        return 'page';
        break;
    default:
        return "please give correct master_api name";
        break;
}

function get_project_folder_list()
{
    $filtered_laravel_folder_array = [];
    $filtered_laravel_folder = [];

    $base_path = dirname(dirname(__FILE__));
    $pieces = explode("\\", $base_path);
    array_pop($pieces);

// C:\xampp\htdocs
    $htdocs_folder_path = implode("\\", $pieces);
    $dir = $htdocs_folder_path;

    if (!file_exists($dir) && !is_dir($dir)) {
        return false;
    }
    $ffs = scandir($dir);
    unset($ffs[array_search('.', $ffs, true)]);
    unset($ffs[array_search('..', $ffs, true)]);

    if (count($ffs) < 1) {
        return;
    }
    foreach ($ffs as $ff) {
        if (is_dir($dir . '/' . $ff)) {
            $array[] = $ff;
        }
    }
    $htdocs_folders = $array;

    foreach ($htdocs_folders as $key => $value) {
        $dir = $htdocs_folder_path . "\\" . $value;
        $ffs = scandir($dir);
        unset($ffs[array_search('.', $ffs, true)]);
        unset($ffs[array_search('..', $ffs, true)]);
        foreach ($ffs as $ff) {
            if (is_dir($dir . '/' . $ff)) {
                // if ($ff == "app") {
                $dir = str_replace($htdocs_folder_path . '\\', '', $dir);
                $filtered_laravel_folder_array[] = (object) array('name' => $dir, 'htdocs_folder_path' => $htdocs_folder_path . '\\' . $dir);
                break;
                // }
            }
        }
    }

    $array = array(
        array('name' => 'core_php_login', 'htdocs_folder_path' => 'F:\xampp\htdocs\core_php_login'),
        array('name' => 'short-eats', 'htdocs_folder_path' => 'F:\xampp\htdocs\short-eats'),
    );

    $data = array('data' => $filtered_laravel_folder_array);
    echo json_encode($data);

}

function get_command_list()
{

    $command = array(
        (object) array('cmd' => 'code .'),
        (object) array('cmd' => 'ng serve'),
        (object) array('cmd' => 'php artisan migrate'),
        (object) array('cmd' => 'php artisan db:seed'),
        (object) array('cmd' => 'php artisan serve'),
        (object) array('cmd' => 'php artisan migrate:refresh --seed'),
        (object) array('cmd' => 'php artisan route:list'),
        (object) array('cmd' => 'php artisan config:clear'),
        (object) array('cmd' => 'php artisan config:cache'),
        (object) array('cmd' => 'git add .'),
        (object) array('cmd' => 'git pull'),
    );

    $data = array('data' => $command);
    echo json_encode($data);
}