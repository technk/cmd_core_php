<?php

if ($_GET['post'] == 'run_command') {
    get_current_run_command();
} elseif ($_GET['post'] == 'run_command_list') {
    get_command_list();
} elseif ($_GET['post'] == 'get_command_file_write') {
    get_command_file_write();
}

function get_current_run_command()
{

    $post_date = file_get_contents("php://input");
    $data = json_decode($post_date);
    $htdocs_folder_path = $data->htdocs_folder_path;
    $command = $data->command;
    $command_prompt = $data->command_prompt;

    if ($command_prompt == 'stop') {
        if (!empty($htdocs_folder_path) && !empty($command)) {
            $execQuery = 'start  cmd.exe --% /k  "cd ' . $htdocs_folder_path . " && " . $command . ' "';
        } else {
            $execQuery = 'start  cmd.exe --% /k  "cd ' . $htdocs_folder_path . ' "';
        }
    } else {
        // $execQuery = 'start /B cmd.exe --% /k  "cd ' . $htdocs_folder_path . " && " . $command . ' && exit "';
        $execQuery = 'start /B cmd.exe --% /k  "cd ' . $htdocs_folder_path . " && " . $command . '"';
    }
    if (substr(php_uname(), 0, 7) == "Windows") {
        pclose(popen("start /B " . $execQuery, "r"));
        // exec($execQuery);
        $data = array('status' => 'Success', 'title' => ucwords($command), 'message' => 'The Function Will Successfully Running Here.!');

        echo json_encode($data);
    } else {
        exec($execQuery . " > /dev/null &");
    }
}

function get_command_list()
{

    $command = array(
        (object) array('cmd' => 'code .'),
        (object) array('cmd' => 'ng serve'),
        (object) array('cmd' => 'php artisan migrate'),
        (object) array('cmd' => 'php artisan serve'),
        (object) array('cmd' => 'php artisan db:seed'),
        (object) array('cmd' => 'php artisan migrate:refresh --seed'),
        (object) array('cmd' => 'php artisan route:list'),
        (object) array('cmd' => 'php artisan config:clear'),
        (object) array('cmd' => 'php artisan config:cache'),
        (object) array('cmd' => 'git add .'),
        (object) array('cmd' => 'git pull'),
    );

    $data = array('data' => $command);
    echo json_encode($data);
}

function create_controller_()
{
    $rules = array(
        array('key' => 'company_id'),
        array('key' => 'product_id'),
        array('key' => 'production_batch_no'),
        array('key' => 'expect_production_quantity'),
        array('key' => 'unit_of_measurement_id'),
        array('key' => 'after_production_quantity'),
        array('key' => 'production_weight'),
        array('key' => 'dispatch_request_date'),
    );

    // $data = 'protected $fillable = array('.
    $fillable_data = "protected \$fillable = [";

    foreach ($rules as $key => $value) {

        $field_name_array[] = "'" . $value['key'] . "'";

    }

    $fillable_implode_data = implode(',', $field_name_array);
    $fillable_data .= $fillable_implode_data;
    $fillable_data .= "];" . PHP_EOL;
    // dd($fillable_data);
    return $fillable_data;
}

function get_command_file_write()
{
    $file_open = 'F:/NK/laravel Project 5.7/test_example/app/Model/push.php';
    $arrap = [];
    for ($i = 1; $i <= 10; $i++) {
        $arrap[] = "'" . $i . "'";
    }
    $List = implode(',', $arrap);
    $dis = 'protected $fillable = [' . $List . '];';
    $txt = $dis . PHP_EOL;

    $lines = file($file_open);
    $last = sizeof($lines) - 1;
    unset($lines[$last]);

    $fp = fopen($file_open, 'w');
    fwrite($fp, implode('', $lines));
    fwrite($fp, $txt);
    fwrite($fp, '}');
    fclose($fp);

}