  <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12" ng-controller="myCtrl">
      <div class="card ">
          <div class="card-body ">
              <h3 class="card-title">First Page</h3>
              <div class="row">
                  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                      <div class="form-group ">
                          <label for="">Select Project</label>
                          <select class="custom-select" ng-model="data.htdocs_folder_path"
                              ng-options="item.htdocs_folder_path as item.name for item in folder">
                              <option disabled selected value="">--Select one--</option>
                          </select>
                      </div>
                  </div>
                  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                      <div class="form-group ">
                          <label for="">Select Command</label>
                          <select class="custom-select" ng-model="data.command"
                              ng-options="item.cmd as item.cmd for item in api_command">
                              <option selected value="">--Select one--</option>
                          </select>
                      </div>
                  </div>
                  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                      <div class="form-group ">
                          <label for="">Run command prompt</label>
                          <select class="custom-select" ng-model="data.command_prompt">
                              <option disabled selected value="">--Select one--</option>
                              <option value="stop"> Stop </option>
                              <option selected value="exit"> Exit </option>
                          </select>
                      </div>
                  </div>
                  <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                      <div class="card-body d-flex justify-content-center">
                          <button type="button" ng-disabled="!data.htdocs_folder_path"
                              class="btn btn-primary btn-sm btn-md btn-lg m-3" ng-click="select_folder(data)"><i
                                  class="fa fa-terminal" aria-hidden="true"></i></button>
                          <button type="button" class="btn btn-secondary btn-sm btn-md btn-lg m-3"
                              ng-click="select_reset()"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>