  <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12" ng-controller="get_component">
      <div class="card ">
          <div class="card-body ">
              <h3 class="card-title">Third Page</h3>
              <div class="row">
                  <table class="table table-striped table-bordered">
                      <thead>
                          <tr>
                              <th scope="col">Columns Name</th>
                              <th scope="col">Data Type</th>
                              <th scope="col">Comment</th>
                              <th scope="col">Validation</th>
                              <th scope="col">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="value in table_columns">
                              <td>
                                  <input type="text" class="form-control" ng-model="value.column_name"
                                      aria-describedby="helpId" placeholder="">
                              </td>
                              <td>
                                  <select class="custom-select" ng-model="value.data_type"
                                      ng-options="item.value as item.name for item in datatypes">
                                      <option disabled value=''>--Select one--</option>
                                  </select>
                              </td>
                              <td><input type="text" class="form-control" ng-model="value.comment"
                                      aria-describedby="helpId" placeholder=""></td>
                              <td>
                                  <div class="justify-content-center">
                                      <button type="button" class="btn btn-primary" data-toggle="modal"
                                          data-target="#exampleModal" data-whatever="@getbootstrap"><i class="fa fa-eye"
                                              aria-hidden="true"></i></button>
                                  </div>
                              </td>
                              <td>
                                  <div class="justify-content-center">
                                      <button type="submit" class="btn btn-sm btn-primary" ng-click="addNew($index)"><i
                                              class="fa fa-plus" aria-hidden="true"></i></button>
                                      <button type="submit" ng-disabled="table_columns.length == 1"
                                          class="btn btn-sm btn-danger" ng-click="reMove($index)"><i class="fa fa-minus"
                                              aria-hidden="true"></i></button>
                                  </div>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
              <div class="justify-content-center">
                  <button type="submit" class="btn btn-sm btn-success" ng-click="addNew($index)"><i class="fa fa-plus"
                          aria-hidden="true"></i>&nbsp;<strong>Generate</strong>
                  </button>
                  <button type="submit" ng-disabled="table_columns.length == 1" class="btn btn-sm btn-danger"
                      ng-click="reMove($index)"><i class="fa fa-refresh"
                          aria-hidden="true"></i>&nbsp;<strong>Reset</strong></button>
              </div>
          </div>
      </div>
  </div>


  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <form>
                      <div class="form-group">
                          <label for="recipient-name" class="col-form-label">Recipient:</label>
                          <input type="text" class="form-control" id="recipient-name">
                      </div>
                      <div class="form-group">
                          <label for="message-text" class="col-form-label">Message:</label>
                          <textarea class="form-control" id="message-text"></textarea>
                      </div>
                  </form>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Send message</button>
              </div>
          </div>
      </div>
  </div>