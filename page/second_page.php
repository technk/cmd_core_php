  <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12" ng-controller="get_header_request">
      <div class="card ">
          <div class="card-body ">
              <h3 class="card-title">Second Page</h3>
              <div class="row">
                  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                      <div class="form-group ">
                          <label for="">Select Method</label>
                          <select class="custom-select" ng-model="data.method"
                              ng-options="item.value as item.method for item in method_list">
                              <option disabled selected value="">--Select one--</option>
                          </select>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div class="form-group ">
                          <label for="">Select Url</label>

                          <input type="text" class="form-control" ng-keyup="post_api_change(data)" ng-model=" data.url">

                      </div>
                  </div>
                  <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                      <div class="card-body d-flex justify-content-center">
                          <button type="button" ng-disabled="!data.url" class="btn btn-primary btn-sm btn-md btn-lg m-3"
                              ng-click="select_method(data)"><i class="fa fa-terminal" aria-hidden="true"></i></button>
                          <button type="button" class="btn btn-secondary btn-sm btn-md btn-lg m-3"><i
                                  class="fa fa-refresh" aria-hidden="true"></i></button>
                      </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group ">
                          <label for="">Enter Token</label>
                          <input type="text" class="form-control" ng-model="data.token" value="Bearer">
                      </div>
                  </div>

                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group ">
                          <label>Url</label>
                          <textarea class="form-control" cols="5" rows="2"> {{data.passing_url_data}}</textarea>
                      </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <table class="table  table-bordered table-responsive-xs">
                          <thead class="thead-inverse">
                              <tr>
                                  <th>Enter Param</th>
                                  <th>Enter Value</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr ng-repeat="v in personalDetails">
                                  <td>
                                      <input type="text" ng-keyup="post_content(personalDetails)" class="form-control"
                                          ng-model="v.param" aria-describedby="helpId" placeholder="Enter param">
                                  </td>
                                  <td>
                                      <input type="text" ng-keyup="post_content(personalDetails)" class="form-control"
                                          ng-model="v.value" aria-describedby="helpId" placeholder="Enter value">
                                  </td>
                                  <td>
                                      <div class="form-group">
                                          <input type="submit" class="btn btn-primary btn-sm" value="Add New"
                                              ng-click="addNew($index)">
                                          <input ng-disabled="personalDetails.length == 1" type="button"
                                              class="btn btn-danger btn-sm" ng-click="reMove($index)" value="Remove">
                                      </div>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
              <div ng-if="json_variable" class="json_variable">
                  <json-formatter open="4" json="{json_variable}"></json-formatter>
              </div>
          </div>
      </div>
  </div>