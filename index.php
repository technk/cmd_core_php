<?php
error_reporting(E_ALL ^ E_NOTICE);
if ($_GET['show'] != null) {
    // var_dump($_GET['show']);
    switch ($_GET['show']) {
        case 'index':
            $page = 'page/first_page.php';
            break;
        case 'second':
            $page = 'page/second_page.php';
            break;
        case 'third':
            $page = 'page/third_page.php';
            break;
        case 'four':
            $page = 'page/four_page.php';
            break;
        case 'basic':
            $page = 'page/system_commands.php';
            break;
        default:
            return "please give correct master_api name";
            break;
    }

} else {
    $page = 'page/first_page.php';
}

?>

<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <link rel="icon" type="picture/png" href="picture/icons/favicon.png" />
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="json-formatter-master/dist/json-formatter.min.css">



</head>

<body ng-app="myApp">


    <!-- <div id="draggable" class="btn btn-primary">
        <i class="fa fa-cogs" aria-hidden="true"></i>
    </div> -->

    <div class="container pt-5">
        <div class="row shadow p-3 mb-3 bg-white rounded">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link <?php echo ($page == 'page/first_page.php') ? 'active' : ''; ?>"
                        href="/cmd_core_php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo ($page == 'page/second_page.php') ? 'active' : ''; ?>"
                        href="index.php?show=second">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo ($page == 'page/third_page.php') ? 'active' : ''; ?>"
                        href="index.php?show=third">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo ($page == 'page/four_page.php') ? 'active' : ''; ?>"
                        href="index.php?show=four">Disabled</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo ($page == 'page/system_commands.php') ? 'active' : ''; ?>"
                        href="index.php?show=basic">Basics</a>
                </li>
            </ul>


            <?php include $page;?>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>


    <script>
    $(function() {
        $("#draggable").draggable({
            containment: 'parent',

            drag: function(event, ui) {
                console.log(event, ui);
                // ui.position.left = Math.min(100, ui.position.left);
            }
        });
    });
    $("#draggable").click(function() {
        $("#panel").slideToggle();
    });
    </script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="angularjs/app.js"></script>
    <script src="angularjs/service.js"></script>
    <script src="json-formatter-master/dist/json-formatter.min.js"></script>


</body>

</html>